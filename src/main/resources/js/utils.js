SpartezUtils = {
    getObject: function (path) {
        var parts = SpartezUtils.splitChild(path);
        if (!parts.parentStr) {
            return window[path];
        }
        return SpartezUtils.getObject(parts.parentStr)[parts.childStr];
    },
    setObject: function (path, replaceObject) {
        var parts = SpartezUtils.splitChild(path);
        return SpartezUtils.getObject(parts.parentStr)[parts.childStr] = replaceObject;
    },
    splitChild: function (path) {
        var lastDotIndex = path.lastIndexOf('.');
        var parentStr = path.substr(0, lastDotIndex);
        var childStr = path.substr(lastDotIndex + 1);
        return {parentStr: parentStr, childStr: childStr};
    },
    createHook: function (originalStr, newFunction) {
        try {
            var original = SpartezUtils.getObject(originalStr);
            if (_.isFunction(original)) {
                SpartezUtils.setObject(originalStr, newFunction);
            } else {
                AJS.$.error("Given object is not a function " + originalStr);
            }
        } catch (e) {
            AJS.$.error("Cant append hook to function " + originalStr);
        }
    },

    /**
     * Modified underscore compose function. This function allows to compose functions with multiple parameters.
     */
    compose: function () {
        var args = arguments;
        var start = args.length - 1;
        return function () {
            var i = start;
            var result = args[start].apply(this, arguments);
            while (i--) result = args[i].apply(this, result);   // args[i].call function replaced by apply
            return result;
        };
    },

    appendHook: function (originalStr, target) {
        var original = SpartezUtils.getObject(originalStr);
        var originalWithHook = _.wrap(original, function (fun) {
            var orig = [].shift.apply(arguments); // remove fun from arguments
            var retVal = orig.apply(this, arguments);
            target();
            return retVal;
        });
        SpartezUtils.createHook(originalStr, originalWithHook);
    },
    compositionHook: function (originalStr, target) {
        var original = getObject(originalStr);
        var composition = _.compose(target, original);
        createHook(originalStr, composition);
    },

    transformParamsBeforeInvocationHook: function (originalStr, fun) {
        var original = getObject(originalStr);
        var composition = compose(original, fun);
        createHook(originalStr, composition);
    }



};