SpartezinitializeTaskLists = function () {
    app.todos.fetch({success: function (ev, models) {
        var commentIds = _.uniq(_.map(models, function (mod) {
            return mod.commentId;
        }));
        _.each(commentIds, function (commentId) {
            SpartezAddCommentContent(commentId);
        });
    }});
};

AJS.$(function () {
    var initializeTaskListsOnce = _.once(SpartezinitializeTaskLists);
    if (window.review) {
        SpartezUtils.appendHook("review.addComment", function () {
            var comments = AJS.$(".comment-list .comment-container");
            AJS.$(".spartez-task").remove();
            var realComments = _.filter(comments, function (comment) {
                return comment.id.match("inlinecomment[0-9]+") || comment.id.match("inlinereply[0-9]+") || comment.id.match("generalcomment[0-9]+") || comment.id.match("generalreply[0-9]+")
                || comment.id.match("revisioncomment[0-9]+") || comment.id.match("revisionreply[0-9]+");
            });
            _.each(realComments, function (com) {
                var commentCls = _.find(com.classList, function (cls) {
                    return cls.match('comment[0-9]+');
                });
                var commentId = commentCls.substr(commentCls.search('[0-9]+'));
                var $actionsList = AJS.$(".comment-actions-inner", com);
                var userKey = _.last(AJS.$(".author .user", com)[0].href.split('/'));
                //TODO: tak samo można wyciagnać jeszcze userName i avatar
                AJS.$($actionsList[0]).append("<li><a class='commentButton spartez-task' data-user-key='" + userKey + "' data-comment-id='" + commentId + "'>Add task</a></li>");
            });
            initializeTaskListsOnce();
        });
    }
});