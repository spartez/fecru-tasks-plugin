AJS.$(function ($) {
    var todosApp;

    var showTasksDialog = function () {
        $(document.body).append(Spartez.tasksDialog({}));
        AJS.dialog2("#demo-dialog").show();

        todosApp || (todosApp = new app.AppView());
    };

    var showTasksSummaryDialog = function () {
        $(document.body).append(Spartez.tasksSummaryDialog({}));
        AJS.dialog2("#demo-dialog").show();

        new summary.AppView({reviews: summary.reviews});
    };

    var tasksAction = window.review ? showTasksDialog : showTasksSummaryDialog;
    AJS.$(document).bind('keyup', 't', tasksAction);
});
SpartezTodosMap = {};
SpartezAddCommentContent = function (commentId, focus) {
    var commentContent = AJS.$("#inlinecommentContent" + commentId);
    if(!commentContent.length){
        commentContent = AJS.$("#revisioncommentContent" + commentId);
    }
    if(!commentContent.length){
        commentContent = AJS.$("#generalcommentContent" + commentId);
    }
    if (AJS.$(".comment-tasks-" + commentId).length == 0) {
        commentContent.after(Spartez.inlineTask({
            commentId: commentId
        }));
        SpartezTodosMap[commentId] = new Todos();
        new app.AppInlineView({
            commentId: commentId,
            todos: SpartezTodosMap[commentId]
        });
    }
    if (focus) {
        AJS.$("#new-todo-" + commentId).focus();
    }
};
AJS.$(function ($) {
    AJS.$(document.body).on('click', ".spartez-task", function (e) {
        var addTaskLink = e.target;
        var commentId = AJS.$(addTaskLink).data("commentId");
        SpartezAddCommentContent(commentId, true);
    });
});