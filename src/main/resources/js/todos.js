/*global Backbone */
var app = app || {};

AJS.$(function () {
    'use strict';

    window.Todos = Backbone.Collection.extend({
        model: app.Todo,
        url: function () {
            if (window.review) {
                return FECRU.pageContext + '/rest/fecru-tasks/1.0/' + review.id() + '/tasks'
            } else {
                return FECRU.pageContext + '/rest/fecru-tasks/1.0/tasks'
            }
        },
        completed: function () {
            return this.where({completed: true});
        },

        remaining: function () {
            return this.where({completed: false});
        },

        nextOrder: function () {
            return this.length ? this.last().get('order') + 1 : 1;
        },

        comparator: 'order'
    });

    app.todos = new Todos();
});
