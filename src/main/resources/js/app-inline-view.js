/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};


AJS.$(function () {
    (function ($) {
        'use strict';

        // The Application
        // ---------------

        // Our overall **AppView** is the top-level piece of UI.
        app.AppInlineView = Backbone.View.extend({

            // Instead of generating a new element, bind to the existing skeleton of
            // the App already present in the HTML.
            // Our template for the line of statistics at the bottom of the app.
            statsTemplate: Spartez.stats,

            // Delegated events for creating new items, and clearing completed ones.
            events: {
                'keypress #new-todo': 'createOnEnter',
                //'click #clear-completed': 'clearCompleted',
                'click #toggle-all': 'toggleAllComplete'
            },

            initialize: function (options) {
                this.todos = options.todos;
                this.commentId = options.commentId;
                this.setElement('#todoapp-' + this.commentId);
                this.allCheckbox = $(this.$('#toggle-all-' + this.commentId)[0]);
                this.allCheckbox.on('click', _.bind(this.createOnEnter, this));
                this.$input = this.$('#new-todo-' + this.commentId);
                this.$input.on('keypress', _.bind(this.createOnEnter, this));
                this.$main = this.$('#main-' + this.commentId);
                this.$list = this.$('#todo-list-' + this.commentId);

                $(document.body).on('click', '#clear-completed', _.bind(this.clearCompleted, this));

                this.listenTo(this.todos, 'add', this.addOne);
                this.listenTo(this.todos, 'reset', this.addAll);
                this.listenTo(this.todos, 'change:completed', this.filterOne);
                this.listenTo(this.todos, 'filter', this.filterAll);
                this.listenTo(this.todos, 'all', this.render);

                // Suppresses 'add' events with {reset: true} and prevents the app view
                // from being re-rendered for every model. Only renders when the 'reset'
                // event is triggered at the end of the fetch.
                this.todos.fetch({
                    reset: true,
                    data: {commentId: this.commentId}
                });
            },

            // Re-rendering the App just means refreshing the statistics -- the rest
            // of the app doesn't change.
            render: function () {
                var completed = this.todos.completed().length;
                var remaining = this.todos.remaining().length;

                if (this.todos.length) {
                    this.$main.show();
                    $('#filters li a')
                        .removeClass('selected')
                        .filter('[href="#/' + (app.TodoFilter || '') + '"]')
                        .addClass('selected');
                } else {
                    this.$main.hide();
                }

                this.allCheckbox.checked = !remaining;
            },

            // Add a single todo item to the list by creating a view for it, and
            // appending its element to the `<ul>`.
            addOne: function (todo) {
                if (todo.get('commentId') == this.commentId) {
                    var view = new app.TodoView({ model: todo });
                    this.$list.append(view.render().el);
                }
            },

            // Add all items in the **Todos** collection at once.
            addAll: function () {
                this.$list.html('');
                this.todos.each(this.addOne, this);
            },

            filterOne: function (todo) {
                todo.trigger('visible');
            },

            filterAll: function () {
                this.todos.each(this.filterOne, this);
            },

            // Generate the attributes for a new Todo item.
            newAttributes: function () {
                return {
                    title: this.$input.val().trim(),
                    order: this.todos.nextOrder(),
                    commentId: this.commentId,
                    completed: false
                };
            },

            // If you hit return in the main input field, create new **Todo** model,
            // persisting it to *localStorage*.
            createOnEnter: function (e) {
                if (e.which === ENTER_KEY && this.$input.val().trim()) {
                    this.todos.create(this.newAttributes(),{success : function(){
                        app.todos.fetch();
                    }});
                    this.$input.val('');
                }
            },

            // Clear all completed todo items, destroying their models.
            clearCompleted: function () {
                _.invoke(this.todos.completed(), 'destroy');
                return false;
            },

            toggleAllComplete: function () {
                var completed = this.allCheckbox.checked;

                this.todos.each(function (todo) {
                    todo.save({
                        completed: completed
                    });
                });
            }
        });
    })(AJS.$);
});