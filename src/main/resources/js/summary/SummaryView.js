/*global Backbone, jQuery, _, ENTER_KEY */
var summary = summary || {};

AJS.$(function () {
    'use strict';
    (function () {
        summary.AppView = Backbone.View.extend({
            el: '#summaryapp',
            initialize: function (options) {
                this.reviews = options.reviews;
                this.$reviews = this.$('.tasks-reviews');
                summary.reviews.fetch({reset: true});

                this.listenTo(this.reviews, 'all', this.render);
            },
            render: function () {
                this.$reviews.html('');
                this.reviews.each(function (review) {
                    var reviewView = new summary.ReviewView({model: review});
                    this.$reviews.append(reviewView.render().el);
                }, this);
            }
        });
    })(AJS.$);
});