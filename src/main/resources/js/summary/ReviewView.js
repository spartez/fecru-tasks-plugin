/*global Backbone, jQuery, _, ENTER_KEY, ESC_KEY */
var summary = summary || {};

AJS.$(function () {
    'use strict';

    summary.ReviewView = Backbone.View.extend({
        tagName: 'li',
        template: Spartez.tasksReview,

        render: function () {
            var params = this.model.toJSON();
            params.reviewLink = CRU.UTIL.urlBase() + "/" + this.model.get('reviewKey');
            params.todo = this.countTodoTasks();
            params.all = this.model.get('tasks').length;
            this.$el.html(this.template(params));
            return this;
        },
        countTodoTasks: function () {
            return _.reduce(this.model.get('tasks'), function(memo, task) {
                if (task.completed) {
                    return memo;
                } else {
                    return memo + 1;
                }
            }, 0, this);
        }
    });
});
