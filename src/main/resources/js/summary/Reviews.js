/*global Backbone */
var summary = summary || {};

AJS.$(function () {
    'use strict';

    window.Reviews = Backbone.Collection.extend({
        model: summary.Review,
        url: function () {
            return FECRU.pageContext + '/rest/fecru-tasks/1.0/tasks'
        },

        comparator: 'key'
    });

    summary.reviews = new Reviews();
});
