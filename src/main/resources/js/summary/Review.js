/*global Backbone */
var summary = summary || {};

AJS.$(function () {
    'use strict';

    summary.Review = Backbone.Model.extend({
        defaults: {
            reviewKey: '',
            tasks: []
        }
    });
});
