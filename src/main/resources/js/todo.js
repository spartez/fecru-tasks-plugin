/*global Backbone */
var app = app || {};

AJS.$(function () {
    'use strict';

    // Todo Model
    // ----------

    // Our basic **Todo** model has `title`, `order`, and `completed` attributes.
    app.Todo = Backbone.Model.extend({
        // Default attributes for the todo
        // and ensure that each todo created has `title` and `completed` keys.
        defaults: {
            title: '',
            completed: false
        },

        // Toggle the `completed` state of this todo item.
        toggle: function () {
            var that = this;
            this.save({
                completed: !this.get('completed')
            }, {success: function () {  //spaghetti code :O mniam mniam
                var commentId = that.get("commentId");
                if (SpartezTodosMap[commentId]) {
                    SpartezTodosMap[commentId].fetch();
                }
                app.todos.fetch();
            }});
        },
        destroy: function () {
            var that = this;
            Backbone.Model.prototype.destroy.call(this, {success:
            function(){
                var commentId = that.get("commentId");
                if (SpartezTodosMap[commentId]) {
                    SpartezTodosMap[commentId].fetch({reset: true});
                }
                app.todos.fetch({reset: true});
            }});
        }
    });
});
