package com.spartez.fecru.plugins.rest;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Review {

    private final Map<Integer, TaskRequest> tasks = new HashMap<Integer, TaskRequest>();
    private final String reviewKey;
    private final String userName;

    public Review(String reviewKey, String userName) {
        this.reviewKey = reviewKey;
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void remove(int id) {
        tasks.remove(id);
    }

    public void put(int id, TaskRequest task) {
        tasks.put(id, task);
    }

    public Collection<TaskRequest> tasks() {
        return tasks.values();
    }

    public ReviewResponse toResponse() {
        return new ReviewResponse(reviewKey, tasks());
    }
}
