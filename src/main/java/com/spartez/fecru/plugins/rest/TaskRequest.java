package com.spartez.fecru.plugins.rest;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class TaskRequest {

    public Integer id;
    public Integer commentId;
    public Boolean completed;
    public int order;
    public String title;

    public TaskRequest() {
    }
}
