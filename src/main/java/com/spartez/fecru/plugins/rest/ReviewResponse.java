package com.spartez.fecru.plugins.rest;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.Collection;

@JsonAutoDetect
public class ReviewResponse {

    public String reviewKey;
    public Collection<TaskRequest> tasks;

    public ReviewResponse(String reviewKey, Collection<TaskRequest> tasks) {
        this.reviewKey = reviewKey;
        this.tasks = tasks;
    }
}
