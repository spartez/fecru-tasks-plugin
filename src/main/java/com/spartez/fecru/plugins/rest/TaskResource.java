package com.spartez.fecru.plugins.rest;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Path("/")
public class TaskResource {

    private final Map<String, Review> reviews = new HashMap<String, Review>();
    private static AtomicInteger seq = new AtomicInteger();
    private final UserManager userManager;

    public TaskResource(UserManager userManager) {
        this.userManager = userManager;
    }

    @GET
    @Path("{repoKey}/tasks")
    public Response getTasks(@PathParam("repoKey") String repoKey, @QueryParam("commentId") Integer commentId) {
        Review review = getOrCreateTasksForReview(repoKey);
        Collection<TaskRequest> toReturn = new ArrayList<TaskRequest>();
        if (commentId != null) {
            for (TaskRequest taskRequest : review.tasks()) {
                if (commentId.equals(taskRequest.commentId)) {
                    toReturn.add(taskRequest);
                }
            }
        } else {
            toReturn.addAll(review.tasks());
        }
        return Response.ok(toReturn).build();
    }

    @GET
    @Path("/tasks")
    public Response getTasks2() {
        ArrayList<ReviewResponse> toReturn = new ArrayList<ReviewResponse>();
        for (Review review : reviews.values()) {
            if (review.getUserName().equals(userManager.getRemoteUser().getUsername())) {
                toReturn.add(review.toResponse());
            }
        }
        return Response.ok(toReturn).build();
    }

    @POST
    @Path("{repoKey}/tasks")
    public Response insertTask(@PathParam("repoKey") String repoKey, TaskRequest task) {
        Review tasks = getOrCreateTasksForReview(repoKey);
        task.id = seq.getAndIncrement();
        tasks.put(task.id, task);
        return Response.status(Response.Status.OK).entity(task).build();
    }

    @PUT
    @Path("{repoKey}/tasks/{id}")
    public Response updateTask(@PathParam("repoKey") String repoKey, @PathParam("id") int id, TaskRequest task) {
        Review tasks = getOrCreateTasksForReview(repoKey);
        tasks.put(id, task);
        return Response.status(Response.Status.OK).entity(task).build();
    }

    @DELETE
    @Path("{repoKey}/tasks/{id}")
    public Response removeTask(@PathParam("repoKey") String repoKey, @PathParam("id") int id) {
        Review tasks = getOrCreateTasksForReview(repoKey);
        tasks.remove(id);
        return Response.status(Response.Status.OK).build();
    }

    private Review getOrCreateTasksForReview(String reviewKey) {
        Review tasksForReview = this.reviews.get(reviewKey);
        final UserProfile remoteUser = userManager.getRemoteUser();
        if (tasksForReview == null) {
            tasksForReview = new Review(reviewKey, remoteUser.getUsername());
            this.reviews.put(reviewKey, tasksForReview);
        }
        return tasksForReview;
    }

}
